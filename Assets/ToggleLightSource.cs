using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleLightSource : MonoBehaviour {
    private bool toggle;
    public void OnToggleButton() {
        if (!toggle) {
            toggle = true;
            this.GetComponent<Light>().enabled = true;
        } else {
            toggle = false;
            this.GetComponent<Light>().enabled = false;
        }
    }
}
