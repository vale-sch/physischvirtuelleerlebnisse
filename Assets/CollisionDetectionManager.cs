using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class CollisionDetectionManager : MonoBehaviour {
    public AudioSource crashSound;
    void OnCollisionEnter(Collision collision) {
        if (!collision.gameObject.GetComponent<OnCollisionExplode>()) return;
        collision.gameObject.transform.GetChild(0).gameObject.SetActive(true);
        collision.gameObject.transform.rotation = Quaternion.Euler(90, 0, 0);
        collision.gameObject.transform.GetChild(0).gameObject.transform.GetChild(0).rotation = Quaternion.Euler(90, 0, Random.Range(90, 180));
        collision.gameObject.GetComponent<Rigidbody>().freezeRotation = true;
        collision.gameObject.GetComponent<MeshRenderer>().enabled = false;
        StartCoroutine(FadeAwayAndDestroy(collision.gameObject.transform.GetChild(0).gameObject.transform.GetChild(0).gameObject));
    }
    IEnumerator FadeAwayAndDestroy(GameObject colorBlob) {
        crashSound.Play();
        Color blob = colorBlob.GetComponent<Image>().material.GetColor("_Color");
        do {
            blob.a -= Time.fixedDeltaTime * 0.075f;
            colorBlob.GetComponent<Image>().material.SetColor("_Color", blob);
            yield return null;
        } while (blob.a > 0.01f);
        blob.a = 1;
        colorBlob.GetComponent<Image>().material.SetColor("_Color", blob);
        Destroy(colorBlob.transform.parent.parent.parent.gameObject);
    }
}
