using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeLight : MonoBehaviour {
    private bool hastToGoUp = true;
    private bool hasToGoDown;
    void Update() {
        if (hasToGoDown) {
            if (this.GetComponent<Light>().spotAngle <= 20) {
                hastToGoUp = true;
                hasToGoDown = false;
            }
            this.GetComponent<Light>().spotAngle -= Time.deltaTime;
        }
        if (hastToGoUp) {
            if (this.GetComponent<Light>().spotAngle >= 30) {
                hastToGoUp = false;
                hasToGoDown = true;
            }
            this.GetComponent<Light>().spotAngle += Time.deltaTime;

        }

    }
}
