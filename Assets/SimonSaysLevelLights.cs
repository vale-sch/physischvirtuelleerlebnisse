using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimonSaysLevelLights : MonoBehaviour
{
    public Material levelUndone;
    public Material levelDone;

    public Renderer currentLight;

    void Start()
    {
        
    }

    public void reachedLevel() {
        currentLight.material = levelDone;
    }

    public void restart() {
        currentLight.material = levelUndone;
    }

}
