using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParentTriggerBool : MonoBehaviour {
    public ChildTriggersBool[] hasTriggered;
    public AudioSource smallWinSound;
    public ShapeBool shapeBool;
    private bool hasSetted;

    void Update() {
        if (hasSetted) return;
        foreach (var item in hasTriggered) {
            if (!item.hasTriggered)
                return;
        }

        shapeBool.hasTouchedGround = true;
        smallWinSound.Play();
        shapeBool.gameObject.GetComponent<Microsoft.MixedReality.Toolkit.UI.ObjectManipulator>().enabled = false;
        shapeBool.gameObject.GetComponent<Microsoft.MixedReality.Toolkit.Input.NearInteractionGrabbable>().enabled = false;
        shapeBool.gameObject.GetComponent<Rigidbody>().useGravity = true;
        hasSetted = true;
    }
}
