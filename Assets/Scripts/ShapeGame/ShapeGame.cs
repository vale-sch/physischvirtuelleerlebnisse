using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShapeGame : MonoBehaviour {
    public AudioController audioController;
    public ShapeBool[] shapeTouchGround;
    public GameObject shapeTemplate;

    public ButtonListener buttonListener;
    public MeshRenderer buttonRenderer;
    public GameObject lightButt;

    public AudioSource winSound;
    private bool hasGameFinished;

    void Update() {
        foreach (var item in shapeTouchGround) {
            if (!item.hasTouchedGround)
                return;
        }
        if (!hasGameFinished) {
            hasGameFinished = true;

            buttonRenderer.material.EnableKeyword("_EMISSION");
            lightButt.SetActive(true);
            StartCoroutine(playSounds());
            buttonListener.gameFinished = true;

            shapeTemplate.GetComponent<MeshRenderer>().material.color = Color.green;

        }
    }
    IEnumerator playSounds() {
        winSound.Play();
        while (winSound.isPlaying) {
            yield return null;
        }
        yield return new WaitForSeconds(1f);
        audioController.Play("ShapeGame2");
        while (audioController.getAudioSource("ShapeGame2").isPlaying) {
            yield return null;
        }
        yield return new WaitForSeconds(1f);
        GameObject.Destroy(this.gameObject);

    }
}
