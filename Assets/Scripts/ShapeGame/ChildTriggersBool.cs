using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChildTriggersBool : MonoBehaviour {
    public string myShapeTrigger;

    public bool hasTriggered;
    void OnTriggerEnter(Collider shape) {
        if (!shape.GetComponent<ShapeBool>()) {
            shape.gameObject.GetComponent<Rigidbody>().AddForce(new Vector3(Random.Range(-4f, 4f), 5f, Random.Range(-4f, 4f)), ForceMode.Impulse);
            return;
        }
        if (shape.GetComponent<ShapeBool>().myShape == myShapeTrigger) {
            hasTriggered = true;
            shape.GetComponent<Rigidbody>().useGravity = true;
        } else
            shape.gameObject.GetComponent<Rigidbody>().AddForce(new Vector3(Random.Range(-4f, 4f), 5f, Random.Range(-4f, 4f)), ForceMode.Impulse);
    }
}