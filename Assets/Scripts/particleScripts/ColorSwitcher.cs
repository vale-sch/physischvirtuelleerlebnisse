using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorSwitcher : MonoBehaviour {
    public ParticleSystem[] ptSystems;
    [HideInInspector]
    public Color newColor;


    public void sumUpColors() {
        foreach (var rend in ptSystems) {
            var main = rend.GetComponent<ParticleSystem>().main;

            main.startColor = new Color((main.startColor.color.r + newColor.r) / 2f, (main.startColor.color.g + newColor.g) / 2f, (main.startColor.color.b + newColor.b) / 2f, 1);
        }
    }
    public void enhanceParticleSystem() {
        this.gameObject.transform.localScale = new Vector3(this.gameObject.transform.localScale.x * 1.2f, this.gameObject.transform.localScale.y * 1.2f, this.gameObject.transform.localScale.z * 1.2f);
        this.gameObject.GetComponent<WalkBehaviourParticle>().walkingSpeed = this.gameObject.GetComponent<WalkBehaviourParticle>().walkingSpeed * 1.15f;
        foreach (var rend in ptSystems) {
            rend.gameObject.transform.localScale = new Vector3(rend.gameObject.transform.localScale.x * 1.2f, rend.gameObject.transform.localScale.y * 1.2f, rend.gameObject.transform.localScale.z * 1.2f);
            if (rend.name == "beams/rays" || rend.name == "smoke") {
                if (rend.name == "beams/rays") {
                    var light = rend.lights;
                    light.intensityMultiplier = light.intensityMultiplier * 1.1f;
                    light.rangeMultiplier = light.rangeMultiplier * 1.1f;
                    light.maxLights += 1;
                }
                var em = rend.emission;
                em.rateOverTime = (int)(em.rateOverTime.constant * 1.2f);

            }

        }
    }
}
