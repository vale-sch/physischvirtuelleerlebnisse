using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleContainer : MonoBehaviour {
    public ButtonListener buttonListener;
    public MeshRenderer buttonRenderer;
    public GameObject lightButt;
    public GameObject lightCone;
    public ColorSwitcher particleSystemCS;
    public AudioController audioController;
    private int counter;

    void OnTriggerEnter(Collider particle) {
        if (!particle.GetComponent<GlowingParticle>()) return;
        buttonListener.gameFinished = true;
        buttonRenderer.material.EnableKeyword("_EMISSION");
        lightButt.SetActive(true);
        lightCone.SetActive(false);
        particleSystemCS.newColor = particle.GetComponent<ParticleSystem>().main.startColor.color;
        particleSystemCS.sumUpColors();
        particleSystemCS.enhanceParticleSystem();
        Destroy(particle.gameObject);
        StartCoroutine(playSounds());

    }
    IEnumerator playSounds() {
        counter++;
        yield return new WaitForSeconds(1f);
        switch (counter) {
            case 1:
                audioController.Play("Partikel1");
                break;
            case 2:
                audioController.Play("Partikel2");
                break;
            case 3:
                audioController.Play("Partikel3");
                break;

        }
    }
}
