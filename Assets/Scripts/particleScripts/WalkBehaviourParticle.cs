﻿using UnityEngine;
using System.Collections;
public class WalkBehaviourParticle : MaybePhysical {

    public AudioController audioController;
    public GameStateManager gameStateManager;
    public Transform translatePointForFieldOfView;
    public Transform translatePointForFirstGame;
    public Transform translatePointForNextGames;
    public Transform translatePointForContainer;
    public Transform translatePointForGoHome;

    [HideInInspector]
    public bool isFirstGame = true;
    [HideInInspector]
    public bool nextGames;
    public Transform[] waypoints;
    uint waypointIndex = 0;

    [Range(2f, 8f)]
    public float walkingSpeed;
    [HideInInspector]
    public bool repeat = true;
    public bool shouldFollowWaypoints;
    [HideInInspector]
    public bool walkToGamesPosition;

    [HideInInspector]
    public bool hasWalkedToPlayer;

    [HideInInspector]
    public bool walkHome;
    private int gameCounter;

    void FixedUpdate() {
        if (shouldFollowWaypoints) {
            if (waypointIndex >= waypoints.Length) return;

            var dir = (waypoints[waypointIndex].position - transform.position);
            float factor = walkingSpeed * Time.fixedDeltaTime / dir.magnitude;

            SetPosition(transform.position + dir.normalized * walkingSpeed / 2 * Time.fixedDeltaTime);

            if (factor >= 0.5f) {
                SetRotation(waypoints[waypointIndex++].rotation);
                if (waypointIndex == waypoints.Length) {
                    waypointIndex = 0;
                    if (!repeat) enabled = false;
                }
            } else SetRotation(Quaternion.Slerp(transform.rotation, waypoints[waypointIndex].rotation, factor));
        } else {
            if (!hasWalkedToPlayer)
                walkTowardsPlayer();
            if (walkToGamesPosition)
                walkTowardsGame();
            if (walkHome)
                walkTowardsHome();
        }

    }
    void walkTowardsHome() {
        Vector3 distance = translatePointForGoHome.position - this.transform.position;
        this.transform.position = Vector3.Lerp(this.transform.position, translatePointForGoHome.position, Time.deltaTime * walkingSpeed / 5);
        if (walkHome && distance.magnitude < 0.1f) {
            walkHome = false;
        }
    }

    void walkTowardsPlayer() {
        Vector3 distance = translatePointForFieldOfView.position - this.transform.position;
        this.transform.position = Vector3.Lerp(this.transform.position, translatePointForFieldOfView.position, Time.deltaTime * walkingSpeed / 5);
        if (!hasWalkedToPlayer && distance.magnitude < 0.1f) {
            StartCoroutine(enableVoice());
        }
    }
    void walkTowardsGame() {
        if (isFirstGame) {
            Vector3 distance = translatePointForFirstGame.position - this.transform.position;
            this.transform.position = Vector3.Lerp(this.transform.position, translatePointForFirstGame.position, Time.deltaTime * walkingSpeed / 5);
            if (distance.magnitude < 0.5f) {
                walkToGamesPosition = false;
                StartCoroutine(gameStateManager.setGames());
                StartCoroutine(playSounds());
            }
        }
        if (nextGames) {
            Vector3 distance = translatePointForNextGames.position - this.transform.position;
            this.transform.position = Vector3.Lerp(this.transform.position, translatePointForNextGames.position, Time.deltaTime * walkingSpeed / 5);
            if (distance.magnitude < 0.5f) {
                walkToGamesPosition = false;
                StartCoroutine(gameStateManager.setGames());
                StartCoroutine(playSounds());
            }
        }

    }

    IEnumerator playSounds() {
        yield return new WaitForSeconds(2f);
        gameCounter++;
        switch (gameCounter) {
            case 1:
                audioController.Play("TresorSpiel1");
                break;
            case 2:
                audioController.Play("ShapeGame1");
                break;
            case 3:
                audioController.Play("SimonSays1");
                break;

        }
    }


    IEnumerator enableVoice() {
        audioController.Play("erklärung1");
        hasWalkedToPlayer = true;
        do {
            yield return null;
        } while (audioController.getAudioSource("erklärung1").isPlaying);
        Debug.Log("JOJO");
        walkToGamesPosition = true;
    }
}
