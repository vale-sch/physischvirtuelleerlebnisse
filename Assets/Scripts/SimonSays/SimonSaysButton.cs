using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimonSaysButton : MonoBehaviour {

    public Material lightMat;
    public Material normalMat;
    public Renderer myR;

    private Vector3 myTP;

    public int myNumber = 99;
    public SimonSaysLogic myLogic;

    public delegate void ClickEvent(int number);
    public event ClickEvent onClick;
    public AudioSource buttonSound;


    public void OnPressed() {
        if (myLogic.player) {
            ClickedColor();
            StartCoroutine(OnUnpressed());
            //transform.position = new Vector3 (myTP.x, -.3f, myTP.z);
            onClick.Invoke(myNumber);
            buttonSound.Play();
        }
    }

    public IEnumerator OnUnpressed() {
        yield return new WaitForSeconds(0.2f);
        UnClickedColor();
        //transform.position = new Vector3 (myTP.x, myTP.y, myTP.z);
    }

    public void ClickedColor() {
        Debug.Log(this.name);
        myR.material = normalMat;

    }

    public void UnClickedColor() {
        myR.material = lightMat;
    }
}