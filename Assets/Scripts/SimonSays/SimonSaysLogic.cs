using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SimonSaysLogic : MonoBehaviour {
    public AudioController audioController;

    public SimonSaysButton[] myButtons;
    public GameObject startButton;
    public ButtonListener buttonListener;
    public MeshRenderer buttonRenderer;
    public GameObject lightButt;
    public List<int> buttonOrder;
    //public float timeToWin = 25f;
    public AudioSource bigWinSound;
    public AudioSource smallWinSound;
    public AudioSource loseSound;

    public SimonSaysLevelLights[] levelLamps;

    [Range(0.5f, 2f)]
    public float showTime = 0.5f;
    [Range(0.5f, 2f)]
    public float pauseTime = 0.5f;
    [Range(0.5f, 2f)]
    public float waitTimeForNextLevel = 0.5f;

    private bool robot = false;
    public bool player = false;

    public int level = 2;
    private int playerLevel = 0;
    private int myRandom;

    void Start() {
        for (int i = 0; i < myButtons.Length; i++) {
            myButtons[i].onClick += ButtonClicked;
            myButtons[i].myNumber = i;
        }
    }

    void Update() {
        if (robot) {
            robot = false;
            StartCoroutine(Robot());
            Debug.Log("Simon Says: Start");
        }
    }

    void ButtonClicked(int number) {
        if (player) {
            if (number == buttonOrder[playerLevel]) {
                playerLevel += 1;
            } else {
                loseSound.Play();
                GameOver();
            }

            if (playerLevel == level) {
                levelLamps[level - 2].reachedLevel();
                level += 1;
                playerLevel = 0;
                smallWinSound.Play();
                player = false;
                robot = true;
            }
        }
    }

    private IEnumerator Robot() {
        if (buttonOrder.Count < 6) {
            yield return new WaitForSeconds(waitTimeForNextLevel);
            for (int i = 0; i < level; i++) {

                if (buttonOrder.Count < level) {
                    myRandom = Random.Range(0, myButtons.Length);
                    buttonOrder.Add(myRandom);
                }
                myButtons[buttonOrder[i]].ClickedColor();
                myButtons[buttonOrder[i]].GetComponent<AudioSource>().Play();
                yield return new WaitForSeconds(showTime);
                myButtons[buttonOrder[i]].UnClickedColor();
                yield return new WaitForSeconds(pauseTime);
            }
            player = true;
            Debug.Log("player now active!");
        } else {
            buttonRenderer.material.EnableKeyword("_EMISSION");
            lightButt.SetActive(true);
            buttonListener.gameFinished = true;
            StartCoroutine(playSounds());
        }
    }
    IEnumerator playSounds() {
        bigWinSound.Play();
        while (bigWinSound.isPlaying) {
            yield return null;
        }
        yield return new WaitForSeconds(1f);
        audioController.Play("SimonSays2");
        while (audioController.getAudioSource("SimonSays2").isPlaying) {
            yield return null;
        }
        yield return new WaitForSeconds(1f);
        GameObject.Destroy(this.gameObject);

    }
    public void startGame() {
        robot = true;
        playerLevel = 0;
        level = 2;
        buttonOrder.Clear();
        startButton.SetActive(false);
        for (int i = 0; i < 5; i++) {
            levelLamps[i].restart();
        }
    }

    private void GameOver() {

        Debug.Log("Game over!");
        startButton.SetActive(true);
        player = false;
        for (int i = 0; i < 5; i++) {
            levelLamps[i].restart();
        }
    }

}
