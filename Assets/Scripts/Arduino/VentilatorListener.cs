using System;
using UnityEngine;
using System.Collections;
public class VentilatorListener : MonoBehaviour {
    public SerialController serialController;
    private bool isChecking;

    public void SendMessageToTurnOn() {
        serialController.SendSerialMessage("FanOn");
    }
    public void SendMessageToTurnOff() {
        serialController.SendSerialMessage("FanOff");
    }
    void Update() {
        if (!isChecking)
            StartCoroutine(checkOfNewMessage());

    }
    IEnumerator checkOfNewMessage() {
        isChecking = true;
        yield return new WaitForSeconds(0.5f);
        string message = serialController.ReadSerialMessage();

        // Check if the message is plain data or a connect/disconnect event.
        if (ReferenceEquals(message, SerialController.SERIAL_DEVICE_CONNECTED))
            Debug.Log("Connection established");

        isChecking = false;
    }
}


