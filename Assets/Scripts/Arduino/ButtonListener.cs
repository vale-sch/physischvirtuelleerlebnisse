using System;
using UnityEngine;
using System.Collections;
public class ButtonListener : MonoBehaviour {
    public SerialController serialController;
    public DayNightBehaviour dayNightBehaviour;
    public MeshRenderer buttonRenderer;
    public GameObject lightButt;


    [HideInInspector]
    public bool gameFinished;
    private bool isChecking;


    // Initialization
    void Start() {
        Debug.Log("Serial Controller in work...");
    }


    void Update() {

        if (!isChecking)
            StartCoroutine(checkOfNewMessage());

    }
    IEnumerator checkOfNewMessage() {
        isChecking = true;
        yield return new WaitForSeconds(0.5f);
        string message = serialController.ReadSerialMessage();

        if (message == null) {
            isChecking = false;
            yield return null;
        }

        // Check if the message is plain data or a connect/disconnect event.
        if (ReferenceEquals(message, SerialController.SERIAL_DEVICE_CONNECTED))
            Debug.Log("Connection established");
        else if (ReferenceEquals(message, SerialController.SERIAL_DEVICE_DISCONNECTED))
            Debug.Log("Connection attempt failed or disconnection detected");
        if (message == "pressedBT" && gameFinished) {
            dayNightBehaviour.changeDayTime();
            gameFinished = false;
            buttonRenderer.material.DisableKeyword("_EMISSION");
            lightButt.SetActive(false);
        }

        isChecking = false;
    }
}


