using System;
using UnityEngine;
using TMPro;
using System.Collections;
public class TresorListener : MonoBehaviour {
    public AudioController audioController;
    public SerialController serialController;
    public ButtonListener buttonListener;
    public AudioSource bigWinSound;
    public AudioSource smallWinSound;

    public MeshRenderer buttonRenderer;
    public GameObject lightButt;
    public TextMeshProUGUI text;
    public MeshRenderer[] tresorLamps;
    public Material greenMat;
    private bool isChecking;
    private bool[] rightSequence = new bool[3];
    private bool success = false;


    void Update() {
        if (!isChecking && !success)
            StartCoroutine(checkOfNewMessage());
    }

    IEnumerator checkOfNewMessage() {
        isChecking = true;
        yield return new WaitForEndOfFrame();
        string message = serialController.ReadSerialMessage();

        if (message == null) {
            isChecking = false;
            yield return null;
        }

        // Check if the message is plain data or a connect/disconnect event.
        if (ReferenceEquals(message, SerialController.SERIAL_DEVICE_CONNECTED))
            Debug.Log("Connection established");
        else if (ReferenceEquals(message, SerialController.SERIAL_DEVICE_DISCONNECTED))
            Debug.Log("Connection attempt failed or disconnection detected");
        if (message != null && message != "__Connected__" && message != "99")
            text.text = message.ToString();
        switch (message) {
            case ("5"):
                if (rightSequence[0] && !rightSequence[1]) {
                    rightSequence[1] = true;
                    smallWinSound.Play();
                    tresorLamps[1].material = greenMat;
                }
                break;
            case ("3"):
                if (rightSequence[0] && rightSequence[1] && !rightSequence[2]) {
                    rightSequence[2] = true;
                    smallWinSound.Play();
                    tresorLamps[2].material = greenMat;
                }

                break;
            case ("2"):
                if (!rightSequence[0]) {
                    rightSequence[0] = true;
                    smallWinSound.Play();
                    tresorLamps[0].material = greenMat;
                }
                break;
        }
        isChecking = false;
        bool allChecked = true;
        foreach (var sequence in rightSequence) {
            if (!sequence) allChecked = false;
        }
        if (allChecked && !success) {
            success = true;
            buttonListener.gameFinished = true;
            buttonRenderer.material.EnableKeyword("_EMISSION");
            lightButt.SetActive(true);


            StartCoroutine(PlaySounds());

        }

    }

    IEnumerator PlaySounds() {
        bigWinSound.Play();
        while (bigWinSound.isPlaying) {
            yield return null;
        }
        yield return new WaitForSeconds(1f);
        audioController.Play("TresorSpiel2");
        while (audioController.getAudioSource("TresorSpiel2").isPlaying) {
            yield return null;
        }
        yield return new WaitForSeconds(1f);
        audioController.Play("TresorSpiel3");
        while (audioController.getAudioSource("TresorSpiel3").isPlaying) {
            yield return null;
        }
        yield return new WaitForSeconds(1f);
        GameObject.Destroy(this.gameObject.transform.parent.parent.parent.parent.gameObject);

    }
}


