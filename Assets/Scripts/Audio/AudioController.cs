using System.Collections;
using UnityEngine;
using System;


public class AudioController : MonoBehaviour {

    public Sound[] sounds;
    public Sound[] grabbingSounds;
    public AudioController secondAudioController;
    [HideInInspector]
    public bool isPlayingSound;


    // Start is called before the first frame update
    void Awake() {
        foreach (var sound in sounds) {
            sound.source = gameObject.AddComponent<AudioSource>();
            sound.source.clip = sound.clip;
            sound.source.volume = sound.volume;
            sound.source.pitch = sound.pitch;
            sound.source.spatialize = sound.isSpatial;
            sound.source.spatialBlend = sound.spatialBlend;
            sound.source.enabled = sound.isEnabled;
            sound.source.loop = sound.loop;
            sound.source.playOnAwake = sound.playOnAwake;

        }
        foreach (var sound in grabbingSounds) {
            sound.source = gameObject.AddComponent<AudioSource>();
            sound.source.clip = sound.clip;
            sound.source.volume = sound.volume;
            sound.source.pitch = sound.pitch;
            sound.source.spatialize = sound.isSpatial;
            sound.source.spatialBlend = sound.spatialBlend;
            sound.source.enabled = sound.isEnabled;
            sound.source.loop = sound.loop;
            sound.source.playOnAwake = sound.playOnAwake;
        }
    }

    public void Play(string name) {
        isPlayingSound = true;
        Sound sound = Array.Find(sounds, sound => sound.name == name);
        if (sound == null) {
            Debug.LogWarning("Sound: " + name + "not found! Check spelling!");
            return;
        }
        sound.source.enabled = true;
        var actualPlayingSound = sound.source;
        StartCoroutine(checkOfPlayingSound(actualPlayingSound));
        sound.source.Play();
    }
    IEnumerator checkOfPlayingSound(AudioSource actualPlayingSound) {
        while (actualPlayingSound.isPlaying) {
            yield return null;
        }
        isPlayingSound = false;
    }
    public void PlayRandomGrabSound() {
        if (isPlayingSound && secondAudioController.isPlayingSound) return;
        int rnd = UnityEngine.Random.Range(0, grabbingSounds.Length);
        grabbingSounds[rnd].source.Play();
        var actualPlayingSound = grabbingSounds[rnd].source;
        StartCoroutine(checkOfPlayingSound(actualPlayingSound));
    }
    public void Stop(string name) {
        Sound sound = Array.Find(sounds, sound => sound.name == name);
        if (sound == null) {
            Debug.LogWarning("Sound: " + name + "not found! Check spelling!");
            return;
        }
        sound.source.enabled = false;
    }
    public AudioSource getAudioSource(string name) {
        Sound sound = Array.Find(sounds, sound => sound.name == name);
        if (sound == null) {
            Debug.LogWarning("Sound: " + name + "not found! Check spelling!");
            Debug.LogWarning("You will get an empty AudioSource");
            return new AudioSource();
        }
        return sound.source;
    }
}
