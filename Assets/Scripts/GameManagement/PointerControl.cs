using Microsoft.MixedReality.Toolkit;
using Microsoft.MixedReality.Toolkit.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;


class PointerControl : MonoBehaviour {
    IMixedRealityInputSystem inputSystem;
    public bool IsGrabEnabled = true;
    public bool IsHandRayEnabled = true;
    public bool IsHeadCursorEnabled = false;
    public bool IsPokeEnabled = true;

    public void Update() {
        if (inputSystem == null) {
            if (!MixedRealityServiceRegistry.TryGetService(out inputSystem)) {
                Debug.LogError("Cannot find input system");
            }
        }
        if (Input.GetKeyDown(KeyCode.Alpha1)) {
            Debug.Log("Use HoloLens 1");
            SetHoloLens1();
        }
        if (Input.GetKeyDown(KeyCode.Alpha2)) {
            Debug.Log("Use HoloLens 2");
            SetHoloLens2();
        }
        if (inputSystem != null) {
            foreach (var item in inputSystem.DetectedInputSources) {
                if (item.SourceType == InputSourceType.Hand) {
                    var mediator = (inputSystem.FocusProvider as FocusProvider)?.PointerMediators[item.SourceId];
                    var myMediator = mediator as MyPointerMediator;
                    if (myMediator != null) {
                        myMediator.IsGrabEnabled = IsGrabEnabled;
                        myMediator.IsHandRayEnabled = IsHandRayEnabled;
                        myMediator.IsHeadGazeEnabled = IsHeadCursorEnabled;
                        myMediator.IsPokeEnabled = IsPokeEnabled;
                    }
                }
            }
        }
    }

    public void SetFingerOnly() {
        IsPokeEnabled = true;
        IsHeadCursorEnabled = false;
        IsHandRayEnabled = false;
        IsGrabEnabled = false;
    }

    public void SetHoloLens2() {
        IsPokeEnabled = true;
        IsHeadCursorEnabled = false;
        IsHandRayEnabled = true;
        IsGrabEnabled = true;
    }

    public void SetHoloLens1() {
        IsPokeEnabled = false;
        IsHeadCursorEnabled = true;
        IsHandRayEnabled = false;
        IsGrabEnabled = false;
    }

}
