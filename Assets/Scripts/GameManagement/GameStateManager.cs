using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStateManager : MonoBehaviour {
    public GameObject[] games;
    public WalkBehaviourParticle walkBehaviourParticle;
    void Start() {
        this.GetComponent<AudioController>().Play("tagMusikFirst");
    }
    public IEnumerator setGames() {
        yield return new WaitForSeconds(2f);
        for (int i = 0; i < games.Length; i++) {
            if (games[i] != null) {
                games[i].SetActive(!games[i].activeSelf);
                if (i == 0) {
                    walkBehaviourParticle.isFirstGame = false;
                    walkBehaviourParticle.nextGames = true;
                }
                break;
            }
        }
    }
}