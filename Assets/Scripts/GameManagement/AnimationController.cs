using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationController : MonoBehaviour {
    public Animation doorAni;
    public WalkBehaviourParticle ptSystem;
    public void startAni() {
        StartCoroutine(WaitForAnimation());
    }
    public IEnumerator WaitForAnimation() {
        doorAni.Play();
        do {
            yield return null;
        } while (doorAni.isPlaying);
        ptSystem.enabled = true;
    }
}
