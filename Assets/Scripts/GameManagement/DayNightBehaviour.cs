using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DayNightBehaviour : MonoBehaviour {
    public AudioController audioController;
    public AudioController ptAudio;

    public Light dLight;
    public GameObject[] lightParticles;
    public WalkBehaviourParticle walkBehaviourParticle;
    public GameObject[] enviroments;
    public GameObject sunMoonRotation;
    public GameObject smallPointerClock;
    public GameObject outDoorObjects;
    public GameObject lightParticleObject;

    public VentilatorListener ventilatorListener;

    private float sunMoonRotFactor;
    private float startIntensity;
    private Color lightColor;
    private Color skyColor;
    private Color equatorColor;
    private Color groundColor;
    private bool isDay;
    private bool isTriggered;
    private float durationFactor;
    private float tintIntensity = 0.5f;
    private bool dayBool;
    private string tagMusik = "tagMusikFirst";
    private string nachtMusik = "nachtMusikFirst";
    private string oldNachtMusik = "nachtMusikFirst";
    private bool isLastNight;
    private bool isHeaven;
    private void Awake() {
        skyColor = RenderSettings.ambientSkyColor;
        equatorColor = RenderSettings.ambientEquatorColor;
        groundColor = RenderSettings.ambientGroundColor;
        lightColor = dLight.color;
        startIntensity = dLight.intensity;
        RenderSettings.ambientIntensity = 1.0f;
        RenderSettings.skybox.SetVector("_Tint", new Vector4(0.5f, 0.5f, 0.5f, 0.5f));

    }
    public void changeDayTime() {
        sunMoonRotFactor = 0;
        if (isHeaven) {
            outDoorObjects.gameObject.SetActive(false);
            dayBool = false;
            changeToDay();
            return;

        }
        if (!dayBool) {
            dayBool = true;
            changeToNight();
        } else {
            dayBool = false;
            changeToDay();
        }
        smallPointerClock.GetComponent<AudioSource>().Play();
    }

    private void changeToDay() {

        if (!isTriggered) {
            for (int i = 0; i < enviroments.Length; i++) {
                if (enviroments[i].activeSelf) {
                    switch (i) {
                        case 0:
                            oldNachtMusik = nachtMusik;
                            tagMusik = "tagMusikSecond";
                            nachtMusik = "nachtMusikSecond";
                            break;
                        case 1:
                            oldNachtMusik = nachtMusik;
                            tagMusik = "tagMusikThird";
                            nachtMusik = "nachtMusikThird";
                            break;
                    }
                    enviroments[i].SetActive(!enviroments[i].activeSelf);
                    if (!isHeaven)
                        if (enviroments[i + 1])
                            enviroments[i + 1].SetActive(!enviroments[i].activeSelf);
                    if (i + 1 == 1) {
                        sunMoonRotation.gameObject.SetActive(false);
                        StartCoroutine(playSoundsWüste());
                    }
                    if (i + 1 == 2) {
                        StartCoroutine(playSoundsWald());
                        isLastNight = true;
                        if (ventilatorListener.enabled)
                            ventilatorListener.SendMessageToTurnOn();
                    }
                    break;
                }
            }
            audioController.Play(tagMusik);
            isDay = true;
            isTriggered = true;
        }
    }
    IEnumerator playSoundsWüste() {
        yield return new WaitForSeconds(3f);
        ptAudio.Play("Wueste1");
        while (ptAudio.getAudioSource("Wueste1").isPlaying) {
            yield return null;
        }
        ptAudio.Play("Wueste2");
        while (ptAudio.getAudioSource("Wueste2").isPlaying) {
            yield return null;
        }
        ptAudio.Play("Wueste3");
        while (ptAudio.getAudioSource("Wueste3").isPlaying) {
            yield return null;
        }
    }
    IEnumerator playSoundsWald() {
        yield return new WaitForSeconds(3f);
        ptAudio.Play("Wald1");
        while (ptAudio.getAudioSource("Wald1").isPlaying) {
            yield return null;
        }

    }
    private void changeToNight() {

        if (!isTriggered) {
            isDay = false;
            audioController.Play(nachtMusik);
            isTriggered = true;
            if (!isLastNight)
                walkBehaviourParticle.shouldFollowWaypoints = true;

        }

    }
    void FixedUpdate() {
        if (isTriggered) {
            durationFactor = Time.fixedDeltaTime * 0.033f;
            if (isDay) {
                if (RenderSettings.ambientIntensity <= 1.0f) {
                    audioController.getAudioSource(oldNachtMusik).volume -= durationFactor;
                    if (audioController.getAudioSource(tagMusik).volume <= 0.1f)
                        audioController.getAudioSource(tagMusik).volume += durationFactor;


                    RenderSettings.ambientIntensity += durationFactor;
                    dLight.intensity += durationFactor;
                    tintIntensity += durationFactor / 2f;

                    lightColor.r += durationFactor;
                    lightColor.g += durationFactor / 1.1f;
                    lightColor.b += durationFactor / 1.3f;

                    skyColor.r += durationFactor * 1.33f;
                    skyColor.g += durationFactor * 1.33f;
                    skyColor.b += durationFactor * 1.33f;

                    equatorColor.r += durationFactor / 5f;
                    equatorColor.g += durationFactor / 5f;
                    equatorColor.b += durationFactor / 5f;

                    groundColor.r += durationFactor / 20f;
                    groundColor.g += durationFactor / 20f;
                    groundColor.b += durationFactor / 20f;

                    RenderSettings.ambientSkyColor = skyColor;
                    RenderSettings.ambientEquatorColor = equatorColor;
                    RenderSettings.ambientGroundColor = groundColor;
                    dLight.color = lightColor;
                    sunMoonRotFactor += 0.25f;
                    if (sunMoonRotation)
                        sunMoonRotation.transform.Rotate(Vector3.forward * -sunMoonRotFactor * durationFactor);
                    smallPointerClock.transform.Rotate(Vector3.forward * durationFactor * 180);
                    if (tintIntensity <= 0.51f)
                        RenderSettings.skybox.SetVector("_Tint", new Vector4(tintIntensity, tintIntensity, tintIntensity, 0.5f));
                }

                if (RenderSettings.ambientIntensity >= 0.99f) {
                    RenderSettings.ambientIntensity = 1.0f;
                    dLight.intensity = startIntensity;
                    isTriggered = false;
                    audioController.Stop(nachtMusik);

                    smallPointerClock.GetComponent<AudioSource>().Stop();
                    walkBehaviourParticle.shouldFollowWaypoints = false;
                    walkBehaviourParticle.walkToGamesPosition = true;
                }

            } else {
                if (RenderSettings.ambientIntensity >= 0.01f) {

                    audioController.getAudioSource(tagMusik).volume -= durationFactor;
                    if (audioController.getAudioSource(nachtMusik).volume <= 0.1f)
                        audioController.getAudioSource(nachtMusik).volume += durationFactor;

                    RenderSettings.ambientIntensity -= durationFactor;
                    dLight.intensity -= durationFactor;
                    tintIntensity -= durationFactor / 2f;

                    lightColor.r -= durationFactor;
                    lightColor.g -= durationFactor / 1.1f;
                    lightColor.b -= durationFactor / 1.3f;

                    skyColor.r -= durationFactor * 1.33f;
                    skyColor.g -= durationFactor * 1.33f;
                    skyColor.b -= durationFactor * 1.33f;

                    equatorColor.r -= durationFactor / 5f;
                    equatorColor.g -= durationFactor / 5f;
                    equatorColor.b -= durationFactor / 5f;

                    groundColor.r -= durationFactor / 20f;
                    groundColor.g -= durationFactor / 20f;
                    groundColor.b -= durationFactor / 20f;

                    RenderSettings.ambientSkyColor = skyColor;
                    RenderSettings.ambientEquatorColor = equatorColor;
                    RenderSettings.ambientGroundColor = groundColor;
                    dLight.color = lightColor;
                    sunMoonRotFactor += 0.25f;
                    if (sunMoonRotation)
                        sunMoonRotation.transform.Rotate(Vector3.forward * -sunMoonRotFactor * durationFactor);
                    smallPointerClock.transform.Rotate(Vector3.forward * durationFactor * 180);
                    if (tintIntensity >= 0.075f)
                        RenderSettings.skybox.SetVector("_Tint", new Vector4(tintIntensity, tintIntensity, tintIntensity, 0.5f));

                }
                if (RenderSettings.ambientIntensity <= 0.01f) {
                    isTriggered = false;
                    for (int i = 0; i < lightParticles.Length; i++) {
                        if (lightParticles[i] != null) {
                            lightParticles[i].SetActive(!lightParticles[i].activeSelf);
                            break;
                        }
                    }
                    if (isLastNight) {
                        isHeaven = true;
                        lightParticleObject.SetActive(true);
                        ventilatorListener.SendMessageToTurnOff();
                        StartCoroutine(playWald());

                    }
                    smallPointerClock.GetComponent<AudioSource>().Stop();
                    audioController.Stop(tagMusik);
                }
            }
        }
    }
    IEnumerator playWald() {
        ptAudio.Play("Wald2");
        while (ptAudio.getAudioSource("Wald2").isPlaying) {
            yield return null;
        }
        walkBehaviourParticle.shouldFollowWaypoints = false;
        walkBehaviourParticle.walkHome = true;
    }

}

