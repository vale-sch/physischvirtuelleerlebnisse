using Microsoft.MixedReality.Toolkit.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public class MyPointerMediator : Microsoft.MixedReality.Toolkit.Input.DefaultPointerMediator {
    public bool IsGrabEnabled { get; set; } = true;
    public bool IsPokeEnabled { get; set; } = true;
    public bool IsHandRayEnabled { get; set; } = true;
    public bool IsHeadGazeEnabled { get; set; } = false;


    public override void UpdatePointers() {
        base.UpdatePointers();
        if (!IsPokeEnabled || !IsGrabEnabled) {
            foreach (var item in nearInteractPointers) {
                if (!IsPokeEnabled && item is PokePointer) {
                    item.IsActive = false;
                }
                if (item is SpherePointer && !IsGrabEnabled) {
                    item.IsActive = false;
                }
            }
        }

        if (!IsHandRayEnabled || !IsHeadGazeEnabled) {
            foreach (var item in farInteractPointers) {
                if (!IsHandRayEnabled && item is LinePointer) {
                    item.IsActive = false;
                }
                if (!IsHeadGazeEnabled && item is GGVPointer) {
                    item.IsActive = false;
                }
            }
        }
    }
}
