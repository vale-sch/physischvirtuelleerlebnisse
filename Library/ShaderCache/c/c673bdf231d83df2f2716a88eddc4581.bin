DI  <Q                         _BORDER_LIGHT      _BORDER_LIGHT_USES_HOVER_COLOR     _CLIPPING_PLANE    _DISABLE_ALBEDO_MAP    _HOVER_LIGHT   _HOVER_LIGHT_HIGH      _INNER_GLOW    _PROXIMITY_LIGHT    PH  #ifdef VERTEX
#version 100

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	mediump vec4 _MainTex_ST;
uniform 	mediump float _BorderWidth;
attribute highp vec4 in_POSITION0;
attribute highp vec2 in_TEXCOORD0;
attribute highp vec4 in_TEXCOORD2;
attribute highp vec2 in_TEXCOORD3;
attribute mediump vec3 in_NORMAL0;
varying highp vec4 vs_TEXCOORD0;
varying highp vec3 vs_TEXCOORD2;
varying highp vec3 vs_TEXCOORD3;
varying mediump vec3 vs_COLOR3;
vec4 u_xlat0;
bool u_xlatb0;
vec4 u_xlat1;
bvec4 u_xlatb1;
vec4 u_xlat2;
vec4 u_xlat3;
ivec2 u_xlati3;
bvec4 u_xlatb3;
vec3 u_xlat4;
mediump float u_xlat16_5;
float u_xlat6;
vec2 u_xlat7;
bvec2 u_xlatb7;
float u_xlat12;
float u_xlat18;
bool u_xlatb18;
const int BITWISE_BIT_COUNT = 32;
int op_modi(int x, int y) { return x - y * (x / y); }
ivec2 op_modi(ivec2 a, ivec2 b) { a.x = op_modi(a.x, b.x); a.y = op_modi(a.y, b.y); return a; }
ivec3 op_modi(ivec3 a, ivec3 b) { a.x = op_modi(a.x, b.x); a.y = op_modi(a.y, b.y); a.z = op_modi(a.z, b.z); return a; }
ivec4 op_modi(ivec4 a, ivec4 b) { a.x = op_modi(a.x, b.x); a.y = op_modi(a.y, b.y); a.z = op_modi(a.z, b.z); a.w = op_modi(a.w, b.w); return a; }

int op_and(int a, int b) { int result = 0; int n = 1; for (int i = 0; i < BITWISE_BIT_COUNT; i++) { if ((op_modi(a, 2) != 0) && (op_modi(b, 2) != 0)) { result += n; } a = a / 2; b = b / 2; n = n * 2; if (!(a > 0 && b > 0)) { break; } } return result; }
ivec2 op_and(ivec2 a, ivec2 b) { a.x = op_and(a.x, b.x); a.y = op_and(a.y, b.y); return a; }
ivec3 op_and(ivec3 a, ivec3 b) { a.x = op_and(a.x, b.x); a.y = op_and(a.y, b.y); a.z = op_and(a.z, b.z); return a; }
ivec4 op_and(ivec4 a, ivec4 b) { a.x = op_and(a.x, b.x); a.y = op_and(a.y, b.y); a.z = op_and(a.z, b.z); a.w = op_and(a.w, b.w); return a; }

void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
    u_xlatb0 = in_TEXCOORD3.y<0.0;
    u_xlat6 = dot(hlslcc_mtx4x4unity_ObjectToWorld[0], hlslcc_mtx4x4unity_ObjectToWorld[0]);
    u_xlat1.x = sqrt(u_xlat6);
    u_xlat6 = dot(hlslcc_mtx4x4unity_ObjectToWorld[1], hlslcc_mtx4x4unity_ObjectToWorld[1]);
    u_xlat1.y = sqrt(u_xlat6);
    u_xlat2.xy = u_xlat1.xy * in_TEXCOORD2.xy;
    u_xlat6 = dot(hlslcc_mtx4x4unity_ObjectToWorld[2], hlslcc_mtx4x4unity_ObjectToWorld[2]);
    u_xlat1.z = sqrt(u_xlat6);
    u_xlat2.z = u_xlat1.z * in_TEXCOORD3.x;
    u_xlat0.xyz = (bool(u_xlatb0)) ? u_xlat2.xyz : u_xlat1.xyz;
    u_xlat1.x = u_xlat0.y + u_xlat0.x;
    u_xlat1.x = u_xlat0.z + u_xlat1.x;
    u_xlat7.x = min(u_xlat0.y, u_xlat0.x);
    u_xlat2.w = min(u_xlat0.z, u_xlat7.x);
    u_xlat1.x = u_xlat1.x + (-u_xlat2.w);
    u_xlat7.x = max(u_xlat0.y, u_xlat0.x);
    u_xlat7.x = max(u_xlat0.z, u_xlat7.x);
    u_xlat1.x = (-u_xlat7.x) + u_xlat1.x;
    u_xlat1.x = u_xlat2.w / u_xlat1.x;
    u_xlat1.x = u_xlat1.x * _BorderWidth;
    u_xlat3 = u_xlat0.xyyz * u_xlat0.zxxy;
    u_xlatb7.xy = lessThan(u_xlat3.wxww, u_xlat3.zzzz).xy;
    u_xlatb3 = lessThan(u_xlat3, u_xlat3.wwxx);
    u_xlati3.xy = op_and((ivec2(u_xlatb3.yw) * -1), (ivec2(u_xlatb3.xz) * -1));
    u_xlat3.x = (u_xlati3.x != 0) ? u_xlat1.x : float(_BorderWidth);
    u_xlat3.y = (u_xlati3.y != 0) ? u_xlat1.x : float(_BorderWidth);
    u_xlatb7.x = u_xlatb7.y && u_xlatb7.x;
    u_xlat7.y = (u_xlatb7.x) ? u_xlat1.x : _BorderWidth;
    u_xlat0.w = u_xlat3.y;
    u_xlat7.x = u_xlat0.y;
    u_xlatb1.xw = equal(abs(in_NORMAL0.xxxy), vec4(1.0, 0.0, 0.0, 1.0)).xw;
    u_xlat4.yz = (u_xlatb1.w) ? u_xlat0.zw : u_xlat7.xy;
    u_xlat3.zw = u_xlat0.zy;
    u_xlat4.x = u_xlat0.x;
    u_xlat2.xyz = (u_xlatb1.x) ? u_xlat3.zwx : u_xlat4.xyz;
    u_xlat0.x = min(u_xlat2.y, u_xlat2.x);
    u_xlat6 = max(u_xlat2.y, u_xlat2.x);
    u_xlat0.x = u_xlat0.x / u_xlat6;
    u_xlat0.x = (-u_xlat2.z) * u_xlat0.x + 1.0;
    u_xlat6 = (-u_xlat2.z) + 1.0;
    u_xlat12 = (-u_xlat0.x) + u_xlat6;
    u_xlatb18 = u_xlat2.y<u_xlat2.x;
    vs_TEXCOORD3.xyz = u_xlat2.xyw;
    u_xlat16_5 = (u_xlatb18) ? 0.0 : 1.0;
    vs_TEXCOORD0.z = u_xlat16_5 * u_xlat12 + u_xlat0.x;
    u_xlat0.x = (-u_xlat6) + u_xlat0.x;
    vs_TEXCOORD0.w = u_xlat16_5 * u_xlat0.x + u_xlat6;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat0.xyz = in_POSITION0.yyy * hlslcc_mtx4x4unity_ObjectToWorld[1].xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_ObjectToWorld[0].xyz * in_POSITION0.xxx + u_xlat0.xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_ObjectToWorld[2].xyz * in_POSITION0.zzz + u_xlat0.xyz;
    vs_TEXCOORD2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat18 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat18 = inversesqrt(u_xlat18);
    u_xlat0.xyz = vec3(u_xlat18) * u_xlat0.xyz;
    vs_COLOR3.xyz = u_xlat0.xyz;
    return;
}

#endif
#ifdef FRAGMENT
#version 100

#ifdef GL_FRAGMENT_PRECISION_HIGH
    precision highp float;
#else
    precision mediump float;
#endif
precision highp int;
uniform 	vec4 _Color;
uniform 	mediump float _ClipPlaneSide;
uniform 	vec4 _ClipPlane;
uniform 	vec4 _HoverLightData[20];
uniform 	vec4 _ProximityLightData[12];
uniform 	mediump float _FluentLightIntensity;
uniform 	mediump float _BorderMinValue;
uniform 	mediump float _EdgeSmoothingValue;
uniform 	mediump vec4 _InnerGlowColor;
uniform 	mediump float _InnerGlowPower;
varying highp vec4 vs_TEXCOORD0;
varying highp vec3 vs_TEXCOORD2;
varying mediump vec3 vs_COLOR3;
#define SV_Target0 gl_FragData[0]
vec3 u_xlat0;
bool u_xlatb0;
mediump vec4 u_xlat16_1;
vec3 u_xlat2;
mediump vec4 u_xlat16_2;
mediump vec3 u_xlat16_3;
mediump vec4 u_xlat16_4;
mediump vec3 u_xlat16_5;
vec2 u_xlat6;
mediump vec3 u_xlat16_7;
vec3 u_xlat8;
mediump float u_xlat16_9;
vec3 u_xlat10;
vec2 u_xlat16;
mediump vec2 u_xlat16_17;
float u_xlat18;
bool u_xlatb18;
float u_xlat24;
bool u_xlatb24;
mediump float u_xlat16_25;
mediump float u_xlat16_27;
void main()
{
    u_xlat0.xyz = (-_ClipPlane.xyz) * _ClipPlane.www + vs_TEXCOORD2.xyz;
    u_xlat0.x = dot(u_xlat0.xyz, _ClipPlane.xyz);
    u_xlat0.x = u_xlat0.x * _ClipPlaneSide;
    u_xlat0.x = min(u_xlat0.x, 1.0);
    u_xlatb0 = 0.0<u_xlat0.x;
    u_xlat16_1.x = (u_xlatb0) ? 1.0 : 0.0;
    u_xlat16_9 = _Color.w * u_xlat16_1.x + -0.5;
    u_xlatb0 = u_xlat16_9<0.0;
    if(u_xlatb0){discard;}
    u_xlat0.xyz = (-vs_TEXCOORD2.xyz) + _HoverLightData[2].xyz;
    u_xlat0.x = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat0.x = sqrt(u_xlat0.x);
    u_xlat0.x = u_xlat0.x * _HoverLightData[3].w;
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
    u_xlat0.x = (-u_xlat0.x) + 1.0;
    u_xlat0.x = u_xlat0.x * _HoverLightData[2].w;
    u_xlat8.xyz = u_xlat0.xxx * _HoverLightData[3].xyz;
    u_xlat2.xyz = (-vs_TEXCOORD2.xyz) + _HoverLightData[0].xyz;
    u_xlat2.x = dot(u_xlat2.xyz, u_xlat2.xyz);
    u_xlat2.x = sqrt(u_xlat2.x);
    u_xlat2.x = u_xlat2.x * _HoverLightData[1].w;
    u_xlat2.x = clamp(u_xlat2.x, 0.0, 1.0);
    u_xlat2.x = (-u_xlat2.x) + 1.0;
    u_xlat10.x = u_xlat2.x * _HoverLightData[0].w;
    u_xlat16_9 = u_xlat2.x * _HoverLightData[0].w + u_xlat0.x;
    u_xlat0.xyz = u_xlat10.xxx * _HoverLightData[1].xyz + u_xlat8.xyz;
    u_xlat2.xyz = (-vs_TEXCOORD2.xyz) + _HoverLightData[4].xyz;
    u_xlat24 = dot(u_xlat2.xyz, u_xlat2.xyz);
    u_xlat24 = sqrt(u_xlat24);
    u_xlat24 = u_xlat24 * _HoverLightData[5].w;
    u_xlat24 = clamp(u_xlat24, 0.0, 1.0);
    u_xlat24 = (-u_xlat24) + 1.0;
    u_xlat2.x = u_xlat24 * _HoverLightData[4].w;
    u_xlat16_9 = u_xlat24 * _HoverLightData[4].w + u_xlat16_9;
    u_xlat0.xyz = u_xlat2.xxx * _HoverLightData[5].xyz + u_xlat0.xyz;
    u_xlat2.xyz = (-vs_TEXCOORD2.xyz) + _HoverLightData[6].xyz;
    u_xlat24 = dot(u_xlat2.xyz, u_xlat2.xyz);
    u_xlat24 = sqrt(u_xlat24);
    u_xlat24 = u_xlat24 * _HoverLightData[7].w;
    u_xlat24 = clamp(u_xlat24, 0.0, 1.0);
    u_xlat24 = (-u_xlat24) + 1.0;
    u_xlat2.x = u_xlat24 * _HoverLightData[6].w;
    u_xlat16_9 = u_xlat24 * _HoverLightData[6].w + u_xlat16_9;
    u_xlat0.xyz = u_xlat2.xxx * _HoverLightData[7].xyz + u_xlat0.xyz;
    u_xlat2.xyz = (-vs_TEXCOORD2.xyz) + _HoverLightData[8].xyz;
    u_xlat24 = dot(u_xlat2.xyz, u_xlat2.xyz);
    u_xlat24 = sqrt(u_xlat24);
    u_xlat24 = u_xlat24 * _HoverLightData[9].w;
    u_xlat24 = clamp(u_xlat24, 0.0, 1.0);
    u_xlat24 = (-u_xlat24) + 1.0;
    u_xlat2.x = u_xlat24 * _HoverLightData[8].w;
    u_xlat16_9 = u_xlat24 * _HoverLightData[8].w + u_xlat16_9;
    u_xlat0.xyz = u_xlat2.xxx * _HoverLightData[9].xyz + u_xlat0.xyz;
    u_xlat2.xyz = (-vs_TEXCOORD2.xyz) + _HoverLightData[10].xyz;
    u_xlat24 = dot(u_xlat2.xyz, u_xlat2.xyz);
    u_xlat24 = sqrt(u_xlat24);
    u_xlat24 = u_xlat24 * _HoverLightData[11].w;
    u_xlat24 = clamp(u_xlat24, 0.0, 1.0);
    u_xlat24 = (-u_xlat24) + 1.0;
    u_xlat2.x = u_xlat24 * _HoverLightData[10].w;
    u_xlat16_9 = u_xlat24 * _HoverLightData[10].w + u_xlat16_9;
    u_xlat0.xyz = u_xlat2.xxx * _HoverLightData[11].xyz + u_xlat0.xyz;
    u_xlat2.xyz = (-vs_TEXCOORD2.xyz) + _HoverLightData[12].xyz;
    u_xlat24 = dot(u_xlat2.xyz, u_xlat2.xyz);
    u_xlat24 = sqrt(u_xlat24);
    u_xlat24 = u_xlat24 * _HoverLightData[13].w;
    u_xlat24 = clamp(u_xlat24, 0.0, 1.0);
    u_xlat24 = (-u_xlat24) + 1.0;
    u_xlat2.x = u_xlat24 * _HoverLightData[12].w;
    u_xlat16_9 = u_xlat24 * _HoverLightData[12].w + u_xlat16_9;
    u_xlat0.xyz = u_xlat2.xxx * _HoverLightData[13].xyz + u_xlat0.xyz;
    u_xlat2.xyz = (-vs_TEXCOORD2.xyz) + _HoverLightData[14].xyz;
    u_xlat24 = dot(u_xlat2.xyz, u_xlat2.xyz);
    u_xlat24 = sqrt(u_xlat24);
    u_xlat24 = u_xlat24 * _HoverLightData[15].w;
    u_xlat24 = clamp(u_xlat24, 0.0, 1.0);
    u_xlat24 = (-u_xlat24) + 1.0;
    u_xlat2.x = u_xlat24 * _HoverLightData[14].w;
    u_xlat16_9 = u_xlat24 * _HoverLightData[14].w + u_xlat16_9;
    u_xlat0.xyz = u_xlat2.xxx * _HoverLightData[15].xyz + u_xlat0.xyz;
    u_xlat2.xyz = (-vs_TEXCOORD2.xyz) + _HoverLightData[16].xyz;
    u_xlat24 = dot(u_xlat2.xyz, u_xlat2.xyz);
    u_xlat24 = sqrt(u_xlat24);
    u_xlat24 = u_xlat24 * _HoverLightData[17].w;
    u_xlat24 = clamp(u_xlat24, 0.0, 1.0);
    u_xlat24 = (-u_xlat24) + 1.0;
    u_xlat2.x = u_xlat24 * _HoverLightData[16].w;
    u_xlat16_9 = u_xlat24 * _HoverLightData[16].w + u_xlat16_9;
    u_xlat0.xyz = u_xlat2.xxx * _HoverLightData[17].xyz + u_xlat0.xyz;
    u_xlat2.xyz = (-vs_TEXCOORD2.xyz) + _HoverLightData[18].xyz;
    u_xlat24 = dot(u_xlat2.xyz, u_xlat2.xyz);
    u_xlat24 = sqrt(u_xlat24);
    u_xlat24 = u_xlat24 * _HoverLightData[19].w;
    u_xlat24 = clamp(u_xlat24, 0.0, 1.0);
    u_xlat24 = (-u_xlat24) + 1.0;
    u_xlat2.x = u_xlat24 * _HoverLightData[18].w;
    u_xlat16_9 = u_xlat24 * _HoverLightData[18].w + u_xlat16_9;
    u_xlat0.xyz = u_xlat2.xxx * _HoverLightData[19].xyz + u_xlat0.xyz;
    u_xlat2.xyz = (-vs_TEXCOORD2.xyz) + _ProximityLightData[0].xyz;
    u_xlat16_17.x = dot(vs_COLOR3.xyz, vs_COLOR3.xyz);
    u_xlat16_17.x = inversesqrt(u_xlat16_17.x);
    u_xlat16_3.xyz = u_xlat16_17.xxx * vs_COLOR3.xyz;
    u_xlat16_17.x = ((gl_FrontFacing ? 1 : 0) != 0) ? 1.0 : -1.0;
    u_xlat16_3.xyz = u_xlat16_17.xxx * u_xlat16_3.xyz;
    u_xlat24 = dot(u_xlat2.xyz, u_xlat16_3.xyz);
    u_xlat2.x = u_xlat24 * _ProximityLightData[1].y;
    u_xlat2.x = clamp(u_xlat2.x, 0.0, 1.0);
    u_xlat10.xyz = (-u_xlat16_3.xyz) * abs(vec3(u_xlat24)) + _ProximityLightData[0].xyz;
    u_xlat10.xyz = u_xlat10.xyz + (-vs_TEXCOORD2.xyz);
    u_xlat24 = dot(u_xlat10.xyz, u_xlat10.xyz);
    u_xlat24 = sqrt(u_xlat24);
    u_xlat10.x = log2(u_xlat2.x);
    u_xlat2.x = (-u_xlat2.x) + 1.0;
    u_xlat2.x = u_xlat2.x * _ProximityLightData[0].w;
    u_xlat10.x = u_xlat10.x * 0.25;
    u_xlat10.x = exp2(u_xlat10.x);
    u_xlat10.x = max(u_xlat10.x, _ProximityLightData[1].w);
    u_xlat10.x = u_xlat10.x * _ProximityLightData[1].x;
    u_xlat10.x = u_xlat24 / u_xlat10.x;
    u_xlat10.x = u_xlat10.x + -1.0;
    u_xlat10.x = (-u_xlat10.x);
    u_xlat10.x = clamp(u_xlat10.x, 0.0, 1.0);
    u_xlat18 = u_xlat10.x * -2.0 + 3.0;
    u_xlat10.x = u_xlat10.x * u_xlat10.x;
    u_xlat10.x = u_xlat10.x * u_xlat18;
    u_xlatb18 = u_xlat24>=_ProximityLightData[2].x;
    u_xlat24 = u_xlat24 * _ProximityLightData[1].z;
    u_xlat24 = clamp(u_xlat24, 0.0, 1.0);
    u_xlat18 = u_xlatb18 ? 1.0 : float(0.0);
    u_xlat18 = u_xlat18 * _ProximityLightData[2].y;
    u_xlat10.x = u_xlat18 * u_xlat10.x;
    u_xlat18 = u_xlat2.x * u_xlat10.x;
    u_xlat16_9 = u_xlat10.x * u_xlat2.x + u_xlat16_9;
    u_xlat16_17.x = u_xlat24 + (-_ProximityLightData[4].w);
    u_xlat16_25 = u_xlat24 + (-_ProximityLightData[3].w);
    u_xlat16_27 = (-_ProximityLightData[4].w) + _ProximityLightData[5].w;
    u_xlat16_27 = float(1.0) / u_xlat16_27;
    u_xlat16_17.x = u_xlat16_17.x * u_xlat16_27;
    u_xlat16_17.x = clamp(u_xlat16_17.x, 0.0, 1.0);
    u_xlat16_27 = u_xlat16_17.x * -2.0 + 3.0;
    u_xlat16_17.x = u_xlat16_17.x * u_xlat16_17.x;
    u_xlat16_17.x = u_xlat16_17.x * u_xlat16_27;
    u_xlat16_4 = (-_ProximityLightData[3].wxyz) + _ProximityLightData[4].wxyz;
    u_xlat16_27 = float(1.0) / u_xlat16_4.x;
    u_xlat16_25 = u_xlat16_25 * u_xlat16_27;
    u_xlat16_25 = clamp(u_xlat16_25, 0.0, 1.0);
    u_xlat16_27 = u_xlat16_25 * -2.0 + 3.0;
    u_xlat16_25 = u_xlat16_25 * u_xlat16_25;
    u_xlat16_25 = u_xlat16_25 * u_xlat16_27;
    u_xlat16_4.xyz = vec3(u_xlat16_25) * u_xlat16_4.yzw + _ProximityLightData[3].xyz;
    u_xlat16_5.xyz = (-u_xlat16_4.xyz) + _ProximityLightData[5].xyz;
    u_xlat16_4.xyz = u_xlat16_17.xxx * u_xlat16_5.xyz + u_xlat16_4.xyz;
    u_xlat16_4.xyz = vec3(u_xlat18) * u_xlat16_4.xyz + u_xlat0.xyz;
    u_xlat0.xyz = (-vs_TEXCOORD2.xyz) + _ProximityLightData[6].xyz;
    u_xlat0.x = dot(u_xlat0.xyz, u_xlat16_3.xyz);
    u_xlat8.xyz = (-u_xlat16_3.xyz) * abs(u_xlat0.xxx) + _ProximityLightData[6].xyz;
    u_xlat0.x = u_xlat0.x * _ProximityLightData[7].y;
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
    u_xlat8.xyz = u_xlat8.xyz + (-vs_TEXCOORD2.xyz);
    u_xlat8.x = dot(u_xlat8.xyz, u_xlat8.xyz);
    u_xlat8.x = sqrt(u_xlat8.x);
    u_xlat16.x = log2(u_xlat0.x);
    u_xlat0.x = (-u_xlat0.x) + 1.0;
    u_xlat0.x = u_xlat0.x * _ProximityLightData[6].w;
    u_xlat16.x = u_xlat16.x * 0.25;
    u_xlat16.x = exp2(u_xlat16.x);
    u_xlat16.x = max(u_xlat16.x, _ProximityLightData[7].w);
    u_xlat16.x = u_xlat16.x * _ProximityLightData[7].x;
    u_xlat16.x = u_xlat8.x / u_xlat16.x;
    u_xlat16.x = u_xlat16.x + -1.0;
    u_xlat16.x = (-u_xlat16.x);
    u_xlat16.x = clamp(u_xlat16.x, 0.0, 1.0);
    u_xlat24 = u_xlat16.x * -2.0 + 3.0;
    u_xlat16.x = u_xlat16.x * u_xlat16.x;
    u_xlat16.x = u_xlat16.x * u_xlat24;
    u_xlatb24 = u_xlat8.x>=_ProximityLightData[8].x;
    u_xlat8.x = u_xlat8.x * _ProximityLightData[7].z;
    u_xlat8.x = clamp(u_xlat8.x, 0.0, 1.0);
    u_xlat24 = u_xlatb24 ? 1.0 : float(0.0);
    u_xlat24 = u_xlat24 * _ProximityLightData[8].y;
    u_xlat16.x = u_xlat24 * u_xlat16.x;
    u_xlat24 = u_xlat0.x * u_xlat16.x;
    u_xlat16_9 = u_xlat16.x * u_xlat0.x + u_xlat16_9;
    u_xlat16_17.x = u_xlat8.x + (-_ProximityLightData[10].w);
    u_xlat16_25 = u_xlat8.x + (-_ProximityLightData[9].w);
    u_xlat16_3.x = (-_ProximityLightData[10].w) + _ProximityLightData[11].w;
    u_xlat16_3.x = float(1.0) / u_xlat16_3.x;
    u_xlat16_17.x = u_xlat16_17.x * u_xlat16_3.x;
    u_xlat16_17.x = clamp(u_xlat16_17.x, 0.0, 1.0);
    u_xlat16_3.x = u_xlat16_17.x * -2.0 + 3.0;
    u_xlat16_17.x = u_xlat16_17.x * u_xlat16_17.x;
    u_xlat16_17.x = u_xlat16_17.x * u_xlat16_3.x;
    u_xlat16_2 = (-_ProximityLightData[9].wxyz) + _ProximityLightData[10].wxyz;
    u_xlat16_3.x = float(1.0) / u_xlat16_2.x;
    u_xlat16_25 = u_xlat16_25 * u_xlat16_3.x;
    u_xlat16_25 = clamp(u_xlat16_25, 0.0, 1.0);
    u_xlat16_3.x = u_xlat16_25 * -2.0 + 3.0;
    u_xlat16_25 = u_xlat16_25 * u_xlat16_25;
    u_xlat16_25 = u_xlat16_25 * u_xlat16_3.x;
    u_xlat16_3.xyz = vec3(u_xlat16_25) * u_xlat16_2.yzw + _ProximityLightData[9].xyz;
    u_xlat16_5.xyz = (-u_xlat16_3.xyz) + _ProximityLightData[11].xyz;
    u_xlat16_3.xyz = u_xlat16_17.xxx * u_xlat16_5.xyz + u_xlat16_3.xyz;
    u_xlat16_3.xyz = vec3(u_xlat24) * u_xlat16_3.xyz + u_xlat16_4.xyz;
    u_xlat0.xy = vs_TEXCOORD0.zw + vec2(vec2(_EdgeSmoothingValue, _EdgeSmoothingValue));
    u_xlat16.xy = vs_TEXCOORD0.zw + (-vec2(vec2(_EdgeSmoothingValue, _EdgeSmoothingValue)));
    u_xlat0.xy = (-u_xlat16.xy) + u_xlat0.xy;
    u_xlat0.xy = vec2(1.0, 1.0) / u_xlat0.xy;
    u_xlat6.xy = vs_TEXCOORD0.xy + vec2(-0.5, -0.5);
    u_xlat16.xy = abs(u_xlat6.xy) * vec2(2.0, 2.0) + (-u_xlat16.xy);
    u_xlat6.xy = abs(u_xlat6.xy) + abs(u_xlat6.xy);
    u_xlat16_17.xy = u_xlat6.xy * _InnerGlowColor.ww;
    u_xlat16_17.xy = log2(u_xlat16_17.xy);
    u_xlat16_17.xy = u_xlat16_17.xy * vec2(_InnerGlowPower);
    u_xlat16_17.xy = exp2(u_xlat16_17.xy);
    u_xlat16_17.x = u_xlat16_17.y + u_xlat16_17.x;
    u_xlat16_4.xyz = u_xlat16_17.xxx * _InnerGlowColor.xyz;
    u_xlat0.xy = u_xlat0.xy * u_xlat16.xy;
    u_xlat0.xy = clamp(u_xlat0.xy, 0.0, 1.0);
    u_xlat16.xy = u_xlat0.xy * vec2(-2.0, -2.0) + vec2(3.0, 3.0);
    u_xlat0.xy = u_xlat0.xy * u_xlat0.xy;
    u_xlat0.xy = u_xlat0.xy * u_xlat16.xy;
    u_xlat0.x = max(u_xlat0.y, u_xlat0.x);
    u_xlat16_5.xyz = u_xlat0.xxx * u_xlat16_3.xyz;
    u_xlat16_3.xyz = u_xlat16_3.xyz * vec3(_FluentLightIntensity);
    u_xlat16_17.x = u_xlat0.x * _BorderMinValue;
    u_xlat16_7.xyz = u_xlat16_17.xxx * vec3(_FluentLightIntensity) + _Color.xyz;
    u_xlat16_5.xyz = vec3(u_xlat16_9) * u_xlat16_5.xyz;
    u_xlat16_5.xyz = u_xlat16_5.xyz * vec3(_FluentLightIntensity);
    u_xlat16_5.xyz = u_xlat16_5.xyz * vec3(2.0, 2.0, 2.0) + u_xlat16_7.xyz;
    u_xlat16_1.xzw = u_xlat16_5.xyz * u_xlat16_1.xxx + u_xlat16_4.xyz;
    SV_Target0.xyz = u_xlat16_3.xyz * vec3(u_xlat16_9) + u_xlat16_1.xzw;
    SV_Target0.w = 1.0;
    return;
}

#endif
�                                  