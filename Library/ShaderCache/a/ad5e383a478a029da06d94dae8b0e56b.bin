+  <Q                         _BORDER_LIGHT_REPLACES_ALBEDO      _CLIPPING_SPHERE   _DISABLE_ALBEDO_MAP    _IRIDESCENCE"   _METALLIC_TEXTURE_ALBEDO_CHANNEL_A     _PROXIMITY_LIGHT    ,*  #ifdef VERTEX
#version 100

uniform 	vec3 _WorldSpaceCameraPos;
uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	mediump vec4 _MainTex_ST;
uniform 	mediump float _IridescenceIntensity;
uniform 	mediump float _IridescenceThreshold;
uniform 	mediump float _IridescenceAngle;
uniform lowp sampler2D _IridescentSpectrumMap;
attribute highp vec4 in_POSITION0;
attribute highp vec2 in_TEXCOORD0;
attribute mediump vec3 in_NORMAL0;
varying highp vec2 vs_TEXCOORD0;
varying mediump vec3 vs_COLOR2;
varying highp vec3 vs_TEXCOORD2;
varying mediump vec3 vs_COLOR3;
vec4 u_xlat0;
vec4 u_xlat1;
vec2 u_xlat2;
float u_xlat3;
float u_xlat4;
float u_xlat5;
vec2 u_xlat11;
float u_xlat15;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat0.xyz = (-_WorldSpaceCameraPos.xyz) + hlslcc_mtx4x4unity_ObjectToWorld[3].xyz;
    u_xlat15 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat15 = inversesqrt(u_xlat15);
    u_xlat0.xyz = vec3(u_xlat15) * u_xlat0.xyz;
    u_xlat15 = dot(hlslcc_mtx4x4unity_ObjectToWorld[0].xyz, hlslcc_mtx4x4unity_ObjectToWorld[0].xyz);
    u_xlat15 = inversesqrt(u_xlat15);
    u_xlat1.xyz = vec3(u_xlat15) * hlslcc_mtx4x4unity_ObjectToWorld[0].xyz;
    u_xlat0.x = dot(u_xlat1.xyz, u_xlat0.xyz);
    u_xlat0.x = u_xlat0.x * 0.5 + 0.5;
    u_xlat5 = (-_IridescenceThreshold) + 1.0;
    u_xlat11.x = u_xlat0.x * u_xlat5 + _IridescenceThreshold;
    u_xlat1.x = u_xlat5 * u_xlat0.x;
    u_xlat1.y = float(0.5);
    u_xlat11.y = float(0.5);
    u_xlat0.xyz = texture2DGrad(_IridescentSpectrumMap, u_xlat11.xy, vec4(vec4(0.0, 0.0, 0.0, 0.0)).xy, vec4(vec4(0.0, 0.0, 0.0, 0.0)).xy).xyz;
    u_xlat1.xyz = texture2DGrad(_IridescentSpectrumMap, u_xlat1.xy, vec4(vec4(0.0, 0.0, 0.0, 0.0)).xy, vec4(vec4(0.0, 0.0, 0.0, 0.0)).xy).xyz;
    u_xlat0.xyz = u_xlat0.xyz + (-u_xlat1.xyz);
    u_xlat2.xy = in_TEXCOORD0.xy + vec2(-0.5, -0.5);
    u_xlat3 = sin(_IridescenceAngle);
    u_xlat4 = cos(_IridescenceAngle);
    u_xlat15 = u_xlat2.y * u_xlat3;
    u_xlat15 = u_xlat4 * u_xlat2.x + (-u_xlat15);
    u_xlat15 = u_xlat15 / u_xlat4;
    u_xlat0.xyz = vec3(u_xlat15) * u_xlat0.xyz + u_xlat1.xyz;
    u_xlat0.xyz = u_xlat0.xyz * vec3(vec3(_IridescenceIntensity, _IridescenceIntensity, _IridescenceIntensity));
    vs_COLOR2.xyz = u_xlat0.xyz;
    u_xlat0.xyz = in_POSITION0.yyy * hlslcc_mtx4x4unity_ObjectToWorld[1].xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_ObjectToWorld[0].xyz * in_POSITION0.xxx + u_xlat0.xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_ObjectToWorld[2].xyz * in_POSITION0.zzz + u_xlat0.xyz;
    vs_TEXCOORD2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat15 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat15 = inversesqrt(u_xlat15);
    u_xlat0.xyz = vec3(u_xlat15) * u_xlat0.xyz;
    vs_COLOR3.xyz = u_xlat0.xyz;
    return;
}

#endif
#ifdef FRAGMENT
#version 100

#ifdef GL_FRAGMENT_PRECISION_HIGH
    precision highp float;
#else
    precision mediump float;
#endif
precision highp int;
uniform 	vec4 _Color;
uniform 	mediump float _ClipSphereSide;
uniform 	vec4 hlslcc_mtx4x4_ClipSphereInverseTransform[4];
uniform 	vec4 _ProximityLightData[12];
uniform 	mediump float _FluentLightIntensity;
varying mediump vec3 vs_COLOR2;
varying highp vec3 vs_TEXCOORD2;
varying mediump vec3 vs_COLOR3;
#define SV_Target0 gl_FragData[0]
vec4 u_xlat0;
mediump vec4 u_xlat16_0;
bool u_xlatb0;
mediump vec3 u_xlat16_1;
mediump vec3 u_xlat16_2;
mediump vec4 u_xlat16_3;
mediump vec3 u_xlat16_4;
vec3 u_xlat5;
bool u_xlatb5;
float u_xlat6;
bool u_xlatb6;
mediump vec3 u_xlat16_8;
float u_xlat12;
mediump float u_xlat16_14;
float u_xlat18;
bool u_xlatb18;
mediump float u_xlat16_19;
mediump float u_xlat16_20;
void main()
{
    u_xlat0.xyz = vs_TEXCOORD2.yyy * hlslcc_mtx4x4_ClipSphereInverseTransform[1].xyz;
    u_xlat0.xyz = hlslcc_mtx4x4_ClipSphereInverseTransform[0].xyz * vs_TEXCOORD2.xxx + u_xlat0.xyz;
    u_xlat0.xyz = hlslcc_mtx4x4_ClipSphereInverseTransform[2].xyz * vs_TEXCOORD2.zzz + u_xlat0.xyz;
    u_xlat0.xyz = u_xlat0.xyz + hlslcc_mtx4x4_ClipSphereInverseTransform[3].xyz;
    u_xlat0.x = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat0.x = sqrt(u_xlat0.x);
    u_xlat0.x = u_xlat0.x + -0.5;
    u_xlat0.x = u_xlat0.x * _ClipSphereSide;
    u_xlat0.x = min(u_xlat0.x, 1.0);
    u_xlatb0 = 0.0<u_xlat0.x;
    u_xlat16_1.x = (u_xlatb0) ? _Color.w : 0.0;
    u_xlat16_1.x = u_xlat16_1.x + -0.5;
    u_xlatb6 = u_xlat16_1.x<0.0;
    if(u_xlatb6){discard;}
    u_xlat16_1.xyz = vs_COLOR2.xyz + _Color.xyz;
    u_xlat16_1.xyz = (bool(u_xlatb0)) ? u_xlat16_1.xyz : vec3(0.0, 0.0, 0.0);
    u_xlat0.xyz = (-vs_TEXCOORD2.xyz) + _ProximityLightData[6].xyz;
    u_xlat16_19 = dot(vs_COLOR3.xyz, vs_COLOR3.xyz);
    u_xlat16_19 = inversesqrt(u_xlat16_19);
    u_xlat16_2.xyz = vec3(u_xlat16_19) * vs_COLOR3.xyz;
    u_xlat16_19 = ((gl_FrontFacing ? 1 : 0) != 0) ? 1.0 : -1.0;
    u_xlat16_2.xyz = vec3(u_xlat16_19) * u_xlat16_2.xyz;
    u_xlat0.x = dot(u_xlat0.xyz, u_xlat16_2.xyz);
    u_xlat6 = u_xlat0.x * _ProximityLightData[7].y;
    u_xlat6 = clamp(u_xlat6, 0.0, 1.0);
    u_xlat0.xzw = (-u_xlat16_2.xyz) * abs(u_xlat0.xxx) + _ProximityLightData[6].xyz;
    u_xlat0.xzw = u_xlat0.xzw + (-vs_TEXCOORD2.xyz);
    u_xlat0.x = dot(u_xlat0.xzw, u_xlat0.xzw);
    u_xlat0.x = sqrt(u_xlat0.x);
    u_xlat12 = log2(u_xlat6);
    u_xlat6 = (-u_xlat6) + 1.0;
    u_xlat6 = u_xlat6 * _ProximityLightData[6].w;
    u_xlat12 = u_xlat12 * 0.25;
    u_xlat12 = exp2(u_xlat12);
    u_xlat12 = max(u_xlat12, _ProximityLightData[7].w);
    u_xlat12 = u_xlat12 * _ProximityLightData[7].x;
    u_xlat12 = u_xlat0.x / u_xlat12;
    u_xlat12 = u_xlat12 + -1.0;
    u_xlat12 = (-u_xlat12);
    u_xlat12 = clamp(u_xlat12, 0.0, 1.0);
    u_xlat18 = u_xlat12 * -2.0 + 3.0;
    u_xlat12 = u_xlat12 * u_xlat12;
    u_xlat12 = u_xlat12 * u_xlat18;
    u_xlatb18 = u_xlat0.x>=_ProximityLightData[8].x;
    u_xlat0.x = u_xlat0.x * _ProximityLightData[7].z;
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
    u_xlat18 = u_xlatb18 ? 1.0 : float(0.0);
    u_xlat18 = u_xlat18 * _ProximityLightData[8].y;
    u_xlat12 = u_xlat18 * u_xlat12;
    u_xlat6 = u_xlat6 * u_xlat12;
    u_xlat16_19 = u_xlat0.x + (-_ProximityLightData[10].w);
    u_xlat16_20 = u_xlat0.x + (-_ProximityLightData[9].w);
    u_xlat16_3.x = (-_ProximityLightData[10].w) + _ProximityLightData[11].w;
    u_xlat16_3.x = float(1.0) / u_xlat16_3.x;
    u_xlat16_19 = u_xlat16_19 * u_xlat16_3.x;
    u_xlat16_19 = clamp(u_xlat16_19, 0.0, 1.0);
    u_xlat16_3.x = u_xlat16_19 * -2.0 + 3.0;
    u_xlat16_19 = u_xlat16_19 * u_xlat16_19;
    u_xlat16_19 = u_xlat16_19 * u_xlat16_3.x;
    u_xlat16_3 = (-_ProximityLightData[9].wxyz) + _ProximityLightData[10].wxyz;
    u_xlat16_3.x = float(1.0) / u_xlat16_3.x;
    u_xlat16_20 = u_xlat16_20 * u_xlat16_3.x;
    u_xlat16_20 = clamp(u_xlat16_20, 0.0, 1.0);
    u_xlat16_3.x = u_xlat16_20 * -2.0 + 3.0;
    u_xlat16_20 = u_xlat16_20 * u_xlat16_20;
    u_xlat16_20 = u_xlat16_20 * u_xlat16_3.x;
    u_xlat16_3.xyz = vec3(u_xlat16_20) * u_xlat16_3.yzw + _ProximityLightData[9].xyz;
    u_xlat16_4.xyz = (-u_xlat16_3.xyz) + _ProximityLightData[11].xyz;
    u_xlat16_3.xyz = vec3(u_xlat16_19) * u_xlat16_4.xyz + u_xlat16_3.xyz;
    u_xlat16_3.xyz = vec3(u_xlat6) * u_xlat16_3.xyz;
    u_xlat0.xzw = (-vs_TEXCOORD2.xyz) + _ProximityLightData[0].xyz;
    u_xlat0.x = dot(u_xlat0.xzw, u_xlat16_2.xyz);
    u_xlat5.xyz = (-u_xlat16_2.xyz) * abs(u_xlat0.xxx) + _ProximityLightData[0].xyz;
    u_xlat0.x = u_xlat0.x * _ProximityLightData[1].y;
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
    u_xlat5.xyz = u_xlat5.xyz + (-vs_TEXCOORD2.xyz);
    u_xlat12 = dot(u_xlat5.xyz, u_xlat5.xyz);
    u_xlat12 = sqrt(u_xlat12);
    u_xlat18 = log2(u_xlat0.x);
    u_xlat0.x = (-u_xlat0.x) + 1.0;
    u_xlat0.x = u_xlat0.x * _ProximityLightData[0].w;
    u_xlat18 = u_xlat18 * 0.25;
    u_xlat18 = exp2(u_xlat18);
    u_xlat18 = max(u_xlat18, _ProximityLightData[1].w);
    u_xlat18 = u_xlat18 * _ProximityLightData[1].x;
    u_xlat18 = u_xlat12 / u_xlat18;
    u_xlat18 = u_xlat18 + -1.0;
    u_xlat18 = (-u_xlat18);
    u_xlat18 = clamp(u_xlat18, 0.0, 1.0);
    u_xlat5.x = u_xlat18 * -2.0 + 3.0;
    u_xlat18 = u_xlat18 * u_xlat18;
    u_xlat18 = u_xlat18 * u_xlat5.x;
    u_xlatb5 = u_xlat12>=_ProximityLightData[2].x;
    u_xlat12 = u_xlat12 * _ProximityLightData[1].z;
    u_xlat12 = clamp(u_xlat12, 0.0, 1.0);
    u_xlat5.x = u_xlatb5 ? 1.0 : float(0.0);
    u_xlat5.x = u_xlat5.x * _ProximityLightData[2].y;
    u_xlat18 = u_xlat18 * u_xlat5.x;
    u_xlat5.x = u_xlat0.x * u_xlat18;
    u_xlat16_19 = u_xlat18 * u_xlat0.x + u_xlat6;
    u_xlat16_2.x = u_xlat12 + (-_ProximityLightData[4].w);
    u_xlat16_8.x = u_xlat12 + (-_ProximityLightData[3].w);
    u_xlat16_14 = (-_ProximityLightData[4].w) + _ProximityLightData[5].w;
    u_xlat16_14 = float(1.0) / u_xlat16_14;
    u_xlat16_2.x = u_xlat16_14 * u_xlat16_2.x;
    u_xlat16_2.x = clamp(u_xlat16_2.x, 0.0, 1.0);
    u_xlat16_14 = u_xlat16_2.x * -2.0 + 3.0;
    u_xlat16_2.x = u_xlat16_2.x * u_xlat16_2.x;
    u_xlat16_2.x = u_xlat16_2.x * u_xlat16_14;
    u_xlat16_0 = (-_ProximityLightData[3].wxyz) + _ProximityLightData[4].wxyz;
    u_xlat16_14 = float(1.0) / u_xlat16_0.x;
    u_xlat16_8.x = u_xlat16_14 * u_xlat16_8.x;
    u_xlat16_8.x = clamp(u_xlat16_8.x, 0.0, 1.0);
    u_xlat16_14 = u_xlat16_8.x * -2.0 + 3.0;
    u_xlat16_8.x = u_xlat16_8.x * u_xlat16_8.x;
    u_xlat16_8.x = u_xlat16_8.x * u_xlat16_14;
    u_xlat16_8.xyz = u_xlat16_8.xxx * u_xlat16_0.yzw + _ProximityLightData[3].xyz;
    u_xlat16_4.xyz = (-u_xlat16_8.xyz) + _ProximityLightData[5].xyz;
    u_xlat16_2.xyz = u_xlat16_2.xxx * u_xlat16_4.xyz + u_xlat16_8.xyz;
    u_xlat16_2.xyz = u_xlat5.xxx * u_xlat16_2.xyz + u_xlat16_3.xyz;
    u_xlat16_2.xyz = u_xlat16_2.xyz * vec3(_FluentLightIntensity);
    SV_Target0.xyz = u_xlat16_2.xyz * vec3(u_xlat16_19) + u_xlat16_1.xyz;
    SV_Target0.w = 1.0;
    return;
}

#endif
                                  